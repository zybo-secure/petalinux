#
# This file is the master-slave recipe.
#

SUMMARY = "Simple controller application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://build.config \
       file://comm.h \
       file://control.c \
       file://control.h \
       file://fake-pid.c \
       file://fp-pid.c \
       file://fp-pid.h \
       file://gpio.c \
       file://gpio.h \
       file://linux-timer.c \
	   file://Makefile \
       file://timing.c \
       file://timing.h \
       file://uart.c \
       file://uart.h \
		  "

S = "${WORKDIR}"

do_compile() {
	     oe_runmake
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 controller ${D}${bindir}
}
