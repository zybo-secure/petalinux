# Petalinux

# Formatting:

# First build and export hardware in Vivado

# Import .hdf and config
cd <name>
petalinux-config --get-hw-description=<path/to/.sdk>
# -> Image Packing Configuration -> Root filesystem type
# Set to SD
# -> Image Packing Configuration -> Copy final images to tftpboot -> n

# Build
petalinux-build

# edit build/conf/local.conf
OE_TERMINAL="xterm"

petalinux-config -c rootfs
# -> Filesystem Packages -> base -> util-linux -> util-linux -> y

# Create u-boot image
cd images/linux/
petalinux-package --boot --force --fsbl ./zynq_fsbl.elf --fpga ./zybo_z7_20_wrapper.bit --u-boot
(for the fifo systems, the bitstream used should be the download.bit found in the respective
.sdk/zybo_z7_20_wrapper_hw_platform directory)

# Formatting SD card
sudo fdisk /dev/<sdb>
n,p,1,default,+1G
n,p,2,default,default
sudo mkfs.fat -F 32 -n BOOT -I /dev/<sdb>1

sudo cp images/linux/BOOT.BIN <mount of SD partition 1>
sudo cp images/linux/image.ub <mount of SD partition 1>

# Copy over rootfs
sudo umount /dev/sdX2
sudo dd if=images/linux/rootfs.ext4 of=/dev/sdX2
sync

# resize
sudo resize2fs /dev/sdX2
sync

# Applications:
cd <petalinux-proj>
petalinux-create -t apps --name <name> --enable
cd project-spec/meta-user/recipes-apps/<name>
edit files appropriately (or link to submodule)

# To delete an app you must remove IMAGE_INSTALL_append= "name" from
<petalinux-proj>/project-spec/meta-user/recipes-core/images/petalinux-image.bbappend

# building custom software
petalinux-build -x do_populate_sysroot (once)
petalinux-build -c rootfs (once)
petalinux-build -x package

petalinux-build -c <appname> -x do_clean
petalinux-build -c <appname>
petalinux-build -c <appname> -x do_install

# To run the master-slaves on the board use the following
taskset -c 1 master <miso #> <mosi #> &
taskset -c 1 slave-<program> <mosi #> <miso #>
